package ark.fakerealmserver.rotmg.packets;

import ark.fakerealmserver.rotmg.Packet;
import ark.fakerealmserver.rotmg.PacketType;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

@SuppressWarnings("WeakerAccess")
public class CreateSuccessPacket implements Packet {
    public int objectId;

    public int charId;

    public CreateSuccessPacket(int charId) {
        this.objectId = 25660;
        this.charId = charId;
    }

    @Override
    public PacketType getType() {
        return PacketType.CREATESUCCESS;
    }

    @Override
    public void read(DataInputStream in) throws IOException {
        this.objectId = in.readInt();
        this.charId = in.readInt();
    }

    @Override
    public void write(DataOutputStream out) throws IOException {
        out.writeInt(this.objectId);
        out.writeInt(this.charId);
    }
}
