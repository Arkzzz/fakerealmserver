package ark.fakerealmserver.rotmg.packets;

import ark.fakerealmserver.rotmg.Packet;
import ark.fakerealmserver.rotmg.PacketType;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class LoadPacket implements Packet {
    public int charId;
    public boolean isFromArena;

    @Override
    public PacketType getType() {
        return PacketType.LOAD;
    }

    @Override
    public void read(DataInputStream in) throws IOException {
        this.charId = in.readInt();
        this.isFromArena = in.readBoolean();
    }

    @Override
    public void write(DataOutputStream out) throws IOException {
        out.writeInt(this.charId);
        out.writeBoolean(this.isFromArena);
    }
}
