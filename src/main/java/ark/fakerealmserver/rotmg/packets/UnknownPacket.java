package ark.fakerealmserver.rotmg.packets;

import ark.fakerealmserver.rotmg.Packet;
import ark.fakerealmserver.rotmg.PacketType;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class UnknownPacket implements Packet {
    @Override
    public PacketType getType() {
        return PacketType.UNKNOWN;
    }

    @Override
    public void read(DataInputStream in) throws IOException {

    }

    @Override
    public void write(DataOutputStream out) throws IOException {

    }
}
