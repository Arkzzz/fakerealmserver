package ark.fakerealmserver.rotmg.packets;

import ark.fakerealmserver.rotmg.Packet;
import ark.fakerealmserver.rotmg.PacketType;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class FailurePacket implements Packet {

    public int errorId;

    public String errorDescription;

    public FailurePacket() {
        this.errorId = 0;
        this.errorDescription = "Protocol error";
    }

    @Override
    public PacketType getType() {
        return PacketType.FAILURE;
    }

    @Override
    public void read(DataInputStream in) throws IOException {
        this.errorId = in.readInt();
        this.errorDescription = in.readUTF();
    }

    @Override
    public void write(DataOutputStream out) throws IOException {
        out.writeInt(this.errorId);
        out.writeUTF(this.errorDescription);
    }
}
