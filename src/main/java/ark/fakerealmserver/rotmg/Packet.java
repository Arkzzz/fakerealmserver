package ark.fakerealmserver.rotmg;

import java.io.*;

public interface Packet {
    PacketType getType();
    void read(DataInputStream in) throws IOException;
    void write(DataOutputStream out) throws IOException;
}
