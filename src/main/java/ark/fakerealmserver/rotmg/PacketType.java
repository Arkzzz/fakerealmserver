package ark.fakerealmserver.rotmg;

import ark.fakerealmserver.rotmg.packets.*;

public enum PacketType {
    HELLO((byte)100),
    FAILURE((byte)0),
    MAPINFO((byte)85),
    LOAD((byte)62),
    CREATESUCCESS((byte)44),
    UNKNOWN((byte)255);

    public final byte id;
    PacketType(byte _id) {
        this.id = _id;
    }

    public Packet create() {
        switch (this) {
            case HELLO: return new HelloPacket();
            case FAILURE: return new FailurePacket();
            case MAPINFO: return new MapInfoPacket();
            case LOAD: return new LoadPacket();
            default: return new UnknownPacket();
        }
    }

    public static PacketType fromId(byte id) {
        for (PacketType pt : PacketType.values()) {
            if(pt.id == id) {
                return pt;
            }
        }
        return PacketType.UNKNOWN;
    }
}
