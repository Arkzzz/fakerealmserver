package ark.fakerealmserver;

import ark.fakerealmserver.rotmg.ClientListener;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RealmServer {
    public void start(int port) {
        try {
            ServerSocket listener = new ServerSocket(port);
            System.out.println("Server active and listening on port " + port);
            ExecutorService pool = Executors.newFixedThreadPool(50);
            while (true) {
                pool.execute(new ClientListener(listener.accept()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
